package ru.ekfedorov.tm.api.controller;

public interface ICommandController {

    void displayHelp();

    void displayVersion();

    void displayAbout();

    void exit();

    void showCommands();

    void showArguments();

    void showSystemInfo();

}
