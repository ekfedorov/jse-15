package ru.ekfedorov.tm.api.controller;

import ru.ekfedorov.tm.exception.AbstractException;

public interface ITaskController {

    void showList() throws AbstractException, Exception;

    void create() throws Exception;

    void clear();

    void showTaskByIndex() throws Exception;

    void showTaskByName() throws Exception;

    void showTaskById() throws Exception;

    void removeTaskByIndex() throws Exception;

    void removeTaskByName() throws Exception;

    void removeTaskById() throws Exception;

    void updateTaskByIndex() throws Exception;

    void updateTaskById() throws Exception;

    void startTaskById() throws Exception;

    void startTaskByIndex() throws Exception;

    void startTaskByName() throws Exception;

    void finishTaskById() throws Exception;

    void finishTaskByIndex() throws Exception;

    void finishTaskByName() throws Exception;

    void changeTaskStatusById() throws Exception;

    void changeTaskStatusByIndex() throws Exception;

    void changeTaskStatusByName() throws Exception;

    void findAllByProjectId() throws Exception;

    void bindTaskByProject() throws Exception;

    void unbindTaskFromProject() throws Exception;

}