package ru.ekfedorov.tm.api.repository;

import ru.ekfedorov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    List<Task> findAllByProjectId(String projectId);

    List<Task> removeAllByProjectId(String projectId);

    Task bindTaskByProjectId(String projectId, String taskId);

    Task unbindTaskFromProjectId(String taskId);

    void add(Task task);

    void remove(Task task);

    void clear();

    Task findOneById(String id);

    Task removeOneById(String id);

    Task findOneByIndex(Integer index);

    Task removeOneByIndex(Integer index);

    Task findOneByName(String name);

    Task removeOneByName(String name);

}