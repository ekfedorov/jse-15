package ru.ekfedorov.tm.exception.system;

import ru.ekfedorov.tm.exception.AbstractException;

public class NullComparatorException extends AbstractException {

    public NullComparatorException() throws Exception {
        super("Error! Comparator is null...");
    }

}
