package ru.ekfedorov.tm.controller;

import ru.ekfedorov.tm.api.controller.IProjectController;
import ru.ekfedorov.tm.api.service.IProjectService;
import ru.ekfedorov.tm.api.service.IProjectTaskService;
import ru.ekfedorov.tm.enumerated.Sort;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.exception.system.NullProjectException;
import ru.ekfedorov.tm.model.Project;
import ru.ekfedorov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

public class ProjectController implements IProjectController {

    public final IProjectService projectService;

    public final IProjectTaskService projectTaskService;

    public ProjectController(final IProjectService projectService, IProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void showList() throws Exception {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        List<Project> projects;
        if (isEmpty(sort)) projects = projectService.findAll();
        else {
            final Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            projects = projectService.findAll(sortType.getComparator());
        }
        if (projects.isEmpty()) {
            System.out.println("--- There are no projects ---");
            return;
        }
        int index = 1;
        for (final Project project : projects) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println();
    }

    @Override
    public void create() throws Exception {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        String name = TerminalUtil.nextLine();
        if (isEmpty(name)) name = "project";
        System.out.println("ENTER DESCRIPTION:");
        String description = TerminalUtil.nextLine();
        if (isEmpty(description)) description = "without description";
        final Project project = projectService.add(name, description);
        if (project == null) throw new NullProjectException();
        System.out.println();
    }

    @Override
    public void clear() {
        System.out.println("[CLEAR]");
        projectService.clear();
        System.out.println("--- successfully cleared ---\n");
    }

    @Override
    public void showProjectByIndex() throws Exception {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        if (project == null) throw new NullProjectException();
        showProject(project);
        System.out.println();
    }

    private void showProject(Project project) throws Exception {
        if (project == null) throw new NullProjectException();
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + project.getStatus().getDisplayName());
    }

    @Override
    public void showProjectByName() throws Exception {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findOneByName(name);
        if (project == null) throw new NullProjectException();
        showProject(project);
        System.out.println();
    }

    @Override
    public void showProjectById() throws Exception {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        if (project == null) throw new NullProjectException();
        showProject(project);
        System.out.println();
    }

    @Override
    public void removeProjectByIndex() throws Exception {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.removeOneByIndex(index);
        if (project == null) throw new NullProjectException();
    }

    @Override
    public void removeProjectByName() throws Exception {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.removeOneByName(name);
        if (project == null) throw new NullProjectException();
    }

    @Override
    public void removeProjectById() throws Exception {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.removeOneById(id);
        if (project == null) throw new NullProjectException();
    }

    @Override
    public void updateProjectByIndex() throws Exception {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        if (project == null) throw new NullProjectException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateProjectByIndex(index, name, description);
        if (projectUpdated == null) throw new NullProjectException();
    }

    @Override
    public void updateProjectById() throws Exception {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        if (project == null) throw new NullProjectException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateProjectById(id, name, description);
        if (projectUpdated == null) throw new NullProjectException();
    }

    @Override
    public void startProjectById() throws Exception {
        System.out.println("[START PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.startProjectById(id);
        if (project == null) throw new NullProjectException();
    }

    @Override
    public void startProjectByIndex() throws Exception {
        System.out.println("[START PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.startProjectByIndex(index);
        if (project == null) throw new NullProjectException();
    }

    @Override
    public void startProjectByName() throws Exception {
        System.out.println("[START PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.startProjectByName(name);
        if (project == null) throw new NullProjectException();
    }

    @Override
    public void finishProjectById() throws Exception {
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.finishProjectById(id);
        if (project == null) throw new NullProjectException();
    }

    @Override
    public void finishProjectByIndex() throws Exception {
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.finishProjectByIndex(index);
        if (project == null) throw new NullProjectException();
    }

    @Override
    public void finishProjectByName() throws Exception {
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.finishProjectByName(name);
        if (project == null) throw new NullProjectException();
    }

    @Override
    public void changeProjectStatusById() throws Exception {
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        if (project == null) throw new NullProjectException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Project projectUpdated = projectService.changeProjectStatusById(id, status);
        if (projectUpdated == null) throw new NullProjectException();
    }

    @Override
    public void changeProjectStatusByIndex() throws Exception {
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        if (project == null) throw new NullProjectException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Project projectUpdated = projectService.changeProjectStatusByIndex(index, status);
        if (projectUpdated == null) throw new NullProjectException();
    }

    @Override
    public void changeProjectStatusByName() throws Exception {
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findOneByName(name);
        if (project == null) throw new NullProjectException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Project projectUpdated = projectService.changeProjectStatusByName(name, status);
        if (projectUpdated == null) throw new NullProjectException();
    }

    @Override
    public void removeProjectWithTasksById() throws Exception {
        System.out.println("[REMOVE PROJECT WITH TASKS]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectTaskService.removeProjectById(id);
        if (project == null) throw new NullProjectException();
    }

}
